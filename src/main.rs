mod wg_puncher;
mod wg_puncher_linux;
mod linux_networking;

use wg_puncher_linux::LinuxWgPuncher;
use crate::wg_puncher::{WgPuncher, GenericOs};
use anyhow::Result;

fn main() -> Result<()> {
    let l = LinuxWgPuncher::new()?;
    l.daemonize()?;

    Ok(())
}
