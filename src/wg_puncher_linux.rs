use anyhow::{Result, anyhow, Context };
use crate::wg_puncher::{GenericCrypto, GenericOs, GenericNetworking, WgPuncher};
use crate::wg_puncher::MediationCyrptoBehavior;

use std::convert::TryInto;

use std::thread::spawn;
use std::thread::sleep_ms;

use wireguard_uapi::get::Peer;
use wireguard_uapi::set;
use wireguard_uapi::get::PeerBuilder;

use std::collections::HashMap;

use std::sync::Mutex;
use std::sync::Arc;

use core::default::Default;

use base64::decode;

use std::fmt::Display;
use std::net::UdpSocket;


impl GenericCrypto for LinuxWgPuncher {
}

impl GenericOs for LinuxWgPuncher {
    fn daemonize(self) -> Result<()> {
        //TODO listen(block) on multiple control interfaces
        //depening on configuration, such as network/unixsock/...


        //TODO wouldn't being able to shadow S be cool?
        let s = Arc::new( self );

        //TODO uhm is this maybe possible with .clone()'ing
        //inside the cloasure for nicer looking code?
        let s_share = s.clone();
        spawn( move || Self::manage_unixsock(&s_share));
        let s_share = s.clone();
        spawn( move || Self::manage_unixsock(&s_share));
        
        

        loop {
            sleep_ms(100);
        };
    }

    fn manage_unixsock(s: &Arc<Self>) -> Result<()> {
        loop {
            println!("[+]managing unix socket");
            sleep_ms(999999100);
        };
        Ok(())
    }
    
    fn manage_ip_sock(s: &Arc<Self>) -> Result<()> {
        loop {
            println!("[+]managing ip socket");
            sleep_ms(1009999900);
        };
        Ok(())
    }

    fn peer_cache_add<T: AsRef<[u8]>>(peerlist: Mutex<HashMap<[u8;32],Peer>>, b64_uid: T) -> Result<()> {
        let peerlist = peerlist.lock().unwrap();
        let peer = PeerBuilder::default().build().unwrap();

        //TODO inform the user about his public key being too
        //big or whatever
        let c: [u8; 32] = decode("213")?.try_into().unwrap();
        //peerlist.insert(, peer);
        Ok(())
    }
}

//impl GenericNetworking for LinuxWgPuncher {
//}

impl GenericNetworking for LinuxWgPuncher {
    //TODO is it ok to ask for Display trait instead of String?
    fn connect<S: Display>(&mut self, ip: S, port: S) -> Result<()> {
        self.socket = Some(UdpSocket::bind(format!("{}:{}", ip, port))?);

        match self.mediation_crypto_preferences {
            MediationCyrptoBehavior::PreferEncryption => {

            },
            MediationCyrptoBehavior::ForceEncryption => {},
            MediationCyrptoBehavior::DenyEncryption => {},
        }
        Ok(())
    }

}



pub struct LinuxWgPuncher {
    socket: Option<UdpSocket>,
    mediation_crypto_preferences: MediationCyrptoBehavior,
    peerlist: Mutex<HashMap<[u8;32], Peer>>,
}

//impl WgPuncher for LinuxWgPuncher {
//    fn new() -> Result<LinuxWgPuncher> {
//        Ok(LinuxWgPuncher {})
//    }
//}


impl LinuxWgPuncher {
    pub fn new() -> Result<LinuxWgPuncher> {
        Ok(LinuxWgPuncher {
            socket: None,
            mediation_crypto_preferences: MediationCyrptoBehavior::PreferEncryption,
            peerlist: Mutex::new( HashMap::new() ),
        })
    }
}
