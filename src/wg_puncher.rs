use anyhow::Result;
use std::fmt::Display;
use std::sync::Mutex;
use std::sync::Arc;
use wireguard_uapi::get::Peer;
use std::collections::HashMap;

pub enum MediationCyrptoBehavior {
    ForceEncryption,
    PreferEncryption,
    DenyEncryption,
}

pub trait GenericCrypto {
}

pub trait GenericNetworking {
    fn connect<S: Display>(&mut self, ip: S, port: S) -> Result<()>;
    
}

pub trait GenericOs {
    fn daemonize(self) -> Result<()>;

    fn manage_unixsock(s: &Arc<Self>) -> Result<()>;
    
    fn manage_ip_sock(s: &Arc<Self>) -> Result<()>;
   
    //TODO the public key gets stored twice, once as key value
    //for the lookup and once in struct peer itself ....
    fn peer_cache_add<T: AsRef<[u8]>>(peerlist: Mutex<HashMap<[u8;32],Peer>>, b64_uid: T)
        -> Result<()>;
}

pub trait WgPuncher: GenericCrypto + GenericNetworking + GenericOs {
    //TODO how to archive forcing a function new() that outputs
    //a type that complies with all the traits
    //fn new<T>() -> Result<T>
    //    where T: WgPuncher;
}
