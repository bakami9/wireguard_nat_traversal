# wireguard_nat_traversal

udp NAT hole punching/traversal magic for establishing wireguard sessions between two 
peers which are enclosed behind some sort of NAT


## NOTICE: shit is broken and unfineshed, don't even try using it until
this message gets removed

## TODO's

- loggin
- readme in proper english
- winshit support
- ...


### design decisions

- abstraction layers for operating system specific requests (hopefully
allows for better cross platform support -> porting)

- semi unpriviledged mode -> its possible to set hooks to shellscripts (sudo)
or binaries (plain suid). So this tool itself can run only requiring a generic
unpriviledged network socket. The shellcript/binary then in (case a peer was
iscoverd) gets called with the peer informations as parameters.

- optional public key privacy -> Its possible to exchange the connectivity information
over an asyncrounous encryption channel.

- low on resources ->  this helper tool itself requires very little resources

- daemon -> the tool can be run in the background in an daemonized state and
then be configured over a generic unix socket or other means depending on the
platfrom and the enabled features. This is espeically useful for connections
which endpoints frequently change as the mediating server's peer informations
can be polled/updated in a frequent fashion -> overcome ip changes.

- logging to syslog,file other facilities
